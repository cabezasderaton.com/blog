---
title: "El mail imposible"
date: 2020-07-07T00:00:13+02:00
description: "Pequeña historia personal en la que cuento como un mail estuvo a punto de echar al traste un bonito proyecto digital por no ser el canal adecuado para comunicarse con gente joven"
tags: [
    "email",
    "comunicación",
    "errores",
    "jóvenes",
    "viejos"
]
type: "post"
image: el-mail-imposible-cabezas-de-raton-blog-digital.jpg
---

En el [artículo anterior](https://cabezasderaton.com/post/nuestra-incapacidad-imaginar-futuro/) dejé un cebo de cómo el correo electrónico me había hecho darme cuenta de que estaba viejo a mis 35 años y que como herramienta de comunicación dejaba mucho que desear. En este artículo relato esta pequeña historia de abuelo cebolleta que, a mis *treintaytodos*, me ha hecho de verdad esforzarme en una búsqueda infinita de la empatía. Y, sobre todo, me ha hecho empezar un trabajo apasionante de tratar de entender bien a la gente joven, sus usos y costumbres, por qué cosas que a la generación anterior les encantaban ahora las aborrecen sin explicación aparente. Pero en el fondo, tiene toda la lógica del mundo, lo que pasa es que “los mayores” no somos capaces de ponernos por un segundo en la piel de un *teenager*. <!--more-->

Pongámonos en contexto: debía correr el año 2015 y yo trabajaba en [Inqbarna](http://inqbarna.com/), una fantástica empresa de desarrollo de aplicaciones móviles nativas en Barcelona. Concretamente en la Fundación Palo Alto, un jardín urbano maravilloso que os animo a visitar si pasáis alguna vez por el barrio del Poblenou en la ciudad condal :)

{{< figure src="/images/fundacion-palo-alto-barcelona.jpg" >}}


El caso es que a través de una organización pública nos contactaron para dar una serie de formaciones a chavales entre 16 y 20 años que tenían que decidir qué hacer con su futuro. La idea era hablarles de todo lo que involucra al desarrollo de una aplicación móvil: la idea, conceptualización, documentación, prototipado, diseño, desarrollo, lanzamiento, marketing… Y explicarles eso a unos jóvenes que no tenían claro por dónde tirar a nivel de formación, se antojaba como un buen servicio a la sociedad y, además, íbamos a quedar bien porque tiene pinta que el tema de las apps les iba a generar curiosidad.

Tras una reunión previa con la gente de la institución de enseñanza, la dirección de Inqbarna había acordado una serie de formaciones. Ahora no recuerdo exactamente bien pero fueron aproximadamente media docena de sesiones de un par de horas. Preparamos un poco el guión pensando que podíamos aportar algo a gente que lo necesitaba. En el fondo nos poníamos en su piel y decíamos: “oye, vamos a ver si podemos aportarles un poco de luz porque yo a su edad no tenía ni idea de por dónde tirar”.

Vinieron a una primera sesión y, como era de esperar, el público era muy agradecido. Así que Nacho y yo, que solíamos implicarnos en este tipo de labores mientras los demás programaban sin distracciones, nos vinimos arriba y planeamos un “elige tu propia aventura”. La idea era hacer un brainstorming con los “alumnos” y que ellos eligiesen una temática para empezar a conceptualizar una aplicación móvil. Les explicaríamos cada una de las etapas con ejemplos prácticos y con sus ideas plantearíamos funcionalidades para una app. Si la cosa pintaba bien, incluso podría llegar a desarrollarse en un futuro. El plan no podía fallar.

Así que después de la primera sesión, supongamos que serían quedadas semanales todos los martes, les dijimos que en los próximos días les haríamos llegar 2 propuestas con temáticas diferentes para hacer una app: música o deportes. Dos días después, a golpe de jueves les enviamos un mail comentando las 2 opciones para que en nuestra siguiente cita ya pudiésemos empezar a trabajar en la conceptualización de la nueva app con la temática que hubieran escogido.

Se acaba el jueves y cero respuestas.

El viernes contesta una persona dándonos su preferencia desde una dirección de correo que parecía spam.

El lunes seguimos con el contador de respuestas en uno. De unos doce alumnos, sólo uno había respondido al email. Lo primero, sentimiento de indignación: “encima que hacemos esto por amor al arte y ni se molestan en contestar. Desagradecidos”.  A esto le siguieron un par de exabruptos y algún pensamiento del tipo: “se va a pasar horas extra preparando vuestras clases vuestra santa madre. Yo paso.”

Y así llegamos al martes, día de sesión con los chavales. El mosqueo ya había reposado y comentamos que igual habíamos hecho algo mal. Porque el entusiasmo del primer día no podía ser tan falso. O eso o merecían un Oscar a la mejor interpretación coral… Así que al empezar la sesión les cayó un mini-discurso del estilo de: “el que no quiera estar aquí, no es para nada obligatorio. Nosotros ayudaremos encantados a los que quieran estar, pero si alguien prefiere no estar, no tenemos problema ninguno en que os vayáis, que nosotros no cobramos por esto”. Las caras de los chavales eran un auténtico poema. Así que Nacho les preguntó directamente: “a ver, este discursito viene porque os pedimos vuestra opinión para saber si queríais preparar una app de música o de deportes y de los doce que estáis, sólo ha respondido una persona”. En ese momento las caras de los chavales eran de sorpresa. Parecía que aquella preciosa iniciativa se desmoronaba.

“¿Por dónde decís que habéis enviado la pregunta? Yo no he recibido nada”, apuntó uno de los alumnos. “Yo tampoco”. “Ni yo”. Nos miramos y vemos que las caras tornaban en indignación. Así que en un ambiente totalmente desconcertante les dijimos que les habíamos enviado un correo electrónico a la dirección que nos habían dado de contacto desde la institución. En él explicábamos ambas opciones y pedíamos su respuesta. Y sólo una persona había contestado. Así que recibir, sí que la deberían haber recibido.

Tras un primer momento de cierto ruido, un chaval tiró de sinceridad y nos dijo: “es que yo el email no lo uso nunca. Me lo pidieron para inscribirme al curso pero sólo lo había usado antes para darme de alta en Facebook. Nunca lo leo”. Y ahí empezó la revelación. Hasta había una luz tenue en la sala de reuniones que acompañaba el momento. Otra alumna nos dijo: “a mí nunca me mandan nada interesante al email, así que no entro nunca”. Nuestras caras tenían que ser un poema. Que pena no tener grabada nuestra reacción porque en aquel momento nos sentíamos como verdaderos extraterrestres. “Perdonad, de verdad que nos interesa trabajar sobre la app. Si hace falta me instalo el mail en el móvil, pero de verdad que queremos seguir con este proyecto”, añadió otra. “Yo cada vez que me lo piden me creo uno, porque después nunca lo uso”. En ese momento, además de seguir flipando en colores, ya empezaron las risas y les dijimos que no había absolutamente ningún problema. Había habido un malentendido, pero nada que no se pudiera subsanar.

Así que en lugar de centrarnos en las apps, empezamos preguntando cómo preferían que les contactásemos. Y la respuesta unánime fue Whatsapp. Alguno también hablaba de Facebook Messenger, pero lo que todos miraban era Whatsapp. Y durante un rato siguió la lección, solo que en aquel momento nos la dieron ellos a nosotros y nos explicaron que el mail posiblemente sea una buena herramienta para la gente que trabaja, pero para gente joven no pasa de ser un mal necesario, siendo generosos...

Seguimos con el plan y les dijimos que con calma pensasen si querían música o deporte como temática, que al día siguiente crearíamos un grupo de Whatsapp y a partir de ese momento hablaríamos a través de ese chat. Acabó la sesión y Nacho y yo tuvimos que digerir aquello con unas cañas, porque para dos personas que se jactaban de interesarse por la comunicación, estar al día y hasta creernos un poco interesantes con el rollito de las apps, nos habían hecho quedar como auténticos dinosaurios.

Así que tras un par de cervezas, sin tapa que en Barcelona no se estila, nos empezamos a reír y a pensar que era la primera vez que nos llamaban viejos en nuestra cara. Y lo peor es que tenían razón. Y recuerdo que comentamos: “¿pero de verdad que no le ven ventajas al correo electrónico?” ¿Cómo comunican a varias personas a la vez?” Y después de darle muchas vueltas llegamos a una meridiana conclusión: si no estás trabajando, el mail sólo te vale para registrate en alguna web, red social o plataforma, pero en el día a día no te vale de absolutamente nada.

Al día siguiente creamos el grupo de Whatsapp y el 90% de los participantes había respondido a los 15 minutos. Participación récord y todo el mundo enchufado con el proyecto. ¿Qué había pasado? Muy sencillo, habíamos elegido muy mal el canal de comunicación con los alumnos. Y eso podía haber llevado al traste el proyecto, que era bien bonito. Menos mal que pudimos subsanarlo a tiempo. Y qué gran lección nos llevamos ese día. Desde aquel momento, ya no doy por supuesto que ninguna herramienta de comunicación que yo uso es la buena si el público al que me dirijo puede no estar acostumbrado a utilizarla.

¿Quiere esto decir que el correo electrónico es una mala herramienta? Para nada. Y en nuestro día a día en el trabajo es lo que más usamos. Pero debemos abrir los ojos y ver que algo que para nosotros es inherente a Internet como el email, para las nuevas generaciones no acaba de ser una herramienta de la que saquen provecho. Prefieren las redes sociales, (Slack](https://slack.com/intl/es-es/), el stack de [Atlassian](https://www.atlassian.com/es) o cualquier herramienta que les permita hacer más con menos.

Hay que reconocer que el correo electrónico tiene una serie de desventajas como pueden ser el spam, el mal uso generalizado de la herramienta, la incapacidad de editar un email enviado (aunque hay gratas excepciones a esto), es difícil de escribir de manera estructurada en el móvil, herramienta poco creativa y pensada para texto e imagen. Algo a lo que las nuevas generaciones parecen tener alergia :)

Si en mi post anterior hablaba de que tendemos a imaginarnos el futuro como un presente hipertrofiado, no debemos olvidarnos que el email no deja de ser eso: el correo postal de toda la vida, pero enviado a través de la red de redes. Correo postal inmediato. Que no está nada mal, pero es lo que es: una herramienta antigua adaptada a los tiempos modernos. Y los nativos digitales van a preferir otras herramientas y no debemos asustarnos. Hay que ponerse las pilas y si realmente lo que utilizan es mejor que lo que nosotros conocemos, no queda otra que adaptarse. Bendita evolución.

Pero tampoco hay que caer en lo nuevo por lo nuevo. Se trata de analizar pros y contras para un caso concreto de uso. Hace ya años que empecé a ver artículos en los que se hablaba de [por qué algunas startups dejaban de usar Slack](https://www.fastcompany.com/40433793/my-company-tried-slack-for-two-years-this-is-whywe-quit) o simplemente [abandoban las apps de mensajería instantánea](https://doist.com/blog/betting-against-slack/). Estos artículos son de 2017 y seguro que hay empresas que lo han empezado a utilizar después. Las fechas son lo de menos si sabemos sacar provecho de las herramientas. Para alguien que recibe más de 200 mails al día en 3 cuentas de correo, la gestión se vuelve tediosa. Así que tendré que ponerme las pilas y organizarme mejor.

Se admiten sugerencias sobre herramientas, aunque a día de hoy la que más me apetece probar es [Hey](https://apps.apple.com/us/app/hey-email/id1506603805) de [Jason Fried](https://twitter.com/jasonfried) y su equipo, porque son de lo mejor que le ha pasado al mundo digital últimamente. Aquí dos enlaces con la historia de su app que [primero baneó Apple](https://www.imore.com/hey-email-ceo-responds-apples-decision-reject-it-app-store) antes de [volver a hacer las paces](https://hipertextual.com/2020/06/apple-hey-aprobada-app-store-prueba-gratis).

Por hoy lo dejamos, pero la próxima vez que alguien me haga sentir viejuno, os lo haré saber. Lo bueno es que desde aquel día aprendí que me debo adaptar yo a la gente joven y no al contrario si quiero tener una comunicación real con ellos. Y a partir de ese día se me dio por explorar cosas que mi cabeza desconocía por completo pero que arrasaban entre la juventud como el fenómeno youtuber y los eSports. Pero esto ya son ideas para artículos futuros.

Ya lo decía Led Zeppelin en 1969: [Communication breakdown, it’s always the same…](https://youtu.be/ZnfgRfhdpeQ)

¡Feliz verano!

[**Juan Capeáns**](https://www.linkedin.com/in/juan-capeans)
julio 2020
