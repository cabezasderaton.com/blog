---
title: "Los datos dominan la escena"
date: 2020-06-10T00:21:12+02:00
description: "En la era de los datos tenemos que ser capaces de aprovechar esa información"
tags: [
    "datos",
    "kpis"
]
type: "post"
image: datosrojos.jpg
---

El primer recuerdo que tengo de un dato puede que se remonte a una noticia económica en el telediario. Eran aquellos gráficos llamativos con textos grandes y pocos datos, [(a partir del min 16:20.)](https://www.rtve.es/alacarta/videos/telediario/telediario-2-12-2-1986/5390611/) La utilización y el acceso a los datos ha sufrido un formidable avance los últimos 30 años. <!--more-->Navegas por una web buscando el modelo de zapatillas que tanto te gusta. Realizas la compra en el supermercado. Participas en un sorteo online. En todas estas interacciones estás generando datos que otros pueden explotar. Parece sin embargo que sólo somos conscientes de esa situación cuando de manera explícita cedemos esa información.

**Miguel Escalona**, Científico de datos, explica que “la recolección de datos es omnipresente, solemos asociarla a medios digitales, redes sociales, etc., sin embargo va mucho más allá. Un ejemplo simple que observamos en tiempos de coronavirus es el aforo. Para poder garantizar la reducción de aforo se deben recoger los datos. Luego estos datos pueden ser utilizados por otras plataformas para indicar si un lugar está lleno o cuál es el mejor momento para acudir a ese lugar según mis preferencias y disponibilidad de tiempo”.

Esos datos, en el entorno digital, son una información muy valiosa que los profesionales especializados en su tratamiento tienen que convertir en conocimiento para que la empresa o servicio pueda ofrecerte un trato personalizado y conseguir fidelizarte. **Sergio Simarro**, Asesor en marketing digital, me cuenta que “el uso de datos es básico en su trabajo tanto para realizar propuestas de mejoras basadas en necesidades reales, como para validar que las acciones que he propuesto han sido efectivas.”

Para **Eduardo Sánchez Rojo**, Cofundador de *El arte de medir*, “los datos son un recurso que nos permite explicar y cuantificar mucho de lo que ocurre a nuestro alrededor. Ver la evolución de los números (kms, calorías, euros, etc.) nos permite entender mejor nuestro entorno, nuestro comportamiento y en general todo lo que nos rodea. Y lo mejor es que prácticamente todo lo que nos rodea es medible de una u otra forma. Podemos entender el estado de salud exacto de una piscina en base a una serie de valores químicos del agua, la salubridad del aire, el estado de salud de nuestro propio cuerpo a partir de un análisis de sangre (que no son más que cifras de recuento de ciertos elementos), etc”.

Actividades diarias como ir en coche al trabajo, escribir un WhatsApp a tu pareja o realizar una compra en un comercio producen gran cantidad de datos. Esa producción constante de información puede ayudar a que una ciudad mejore el tráfico o reduzca el tiempo destinado a los desplazamientos, estudiar el grado de emoción que una persona muestra en sus mensajes o reconocer que los gastos que efectúas con tu tarjeta se corresponden a tu historial y que no está siendo usada de forma fraudulenta.

En los últimos años nuevos dispositivos como las pulseras de actividad o los smartwatch ofrecen información sobre nuestra actividad, ejercicio y hábitos de sueño. Con esos datos se nos brinda información de cuestiones médicas o de salud que pueden mejorar nuestro bienestar. Esta lógica puede resultar confusa, datos generados por nosotros y explotados por un tercero pueden mejorar nuestra vida. Los alumnos del ISDI realizaron un [análisis sobre la compra de Fitbit](https://www.isdi.education/es/isdigital-now/blog/alumnos-del-mib-analizan-la-adquisicion-de-google-sobre-fitbit) por parte de Google en 2019. Por un lado el gigante tecnológico quería conseguir una posición destacada en un sector al alza como es el de la salud, y por otro lado y no menos importante consigue una base de datos muy importante que puede utilizar para perfilar y conocer todavía un poco mejor a sus usuarios.


Por eso es difícil explicar que todavía existan empresas digitales que aún no han apostado definitivamente por el uso de los datos. **Sergio Simarro** me comenta que “en las empresas no haya temor hacia la inconsistencia o la falta de datos, la baso en un simple refrán: ojos que no ven, corazón que no siente. Por eso jamás van a tener miedo a los datos, porque posiblemente nunca les hayan prestado atención más allá de la facturación o el beneficio anual”.

Netflix es el paradigma de una compañía que realiza una buena explotación de los datos para mejorar el servicio y el negocio. Pero si lo pensamos fríamente ¿cuántos datos solicita la plataforma para darte de alta? La información más importante ya la irá [recopilando](https://www.datacentric.es/blog/insight/exito-netflix-datos/) de tu comportamiento, de tus búsquedas, de los dispositivos que usas, de tus preferencias, de los contenidos que visualizas, de tus hábitos de consumo en cuanto a días y horas y así hasta un largo análisis del comportamiento de sus clientes.

Si pensamos en las áreas que los datos han supuesto un mayor avance para el ser humano **Miguel Escalona** aporta una visión clara: “esta distribución de conocimiento esta más marcada por la eficiencia monetaria que en los beneficios reales a la humanidad. Esto último puede deberse a que las inversiones principales para el avance de la Inteligencia Artificial esté mayormente realizada por empresas privadas”.

A todos nos ha pasado el ejemplo que contaba al inicio de este post sobre la búsqueda de unas zapatillas nuevas. Se llama [remarketing](https://www.cyberclick.es/numerical-blog/que-es-el-remarketing-funcionamiento-ventajas-tipos), es una técnica muy utilizada para mejorar las ventas de un negocio online. Visitas un producto, esa web guarda cierta información de tu navegación y durante días ese producto “te sigue” en internet. Esta técnica puede generar grandes beneficios para aquellas empresas que sepan realizarla recordando al cliente su necesidad.

Muchos son los ejemplos de la importancia que tiene la correcta explotación de los datos tanto a nivel empresarial, así como el impacto que puede generar en la sociedad. Los profesionales del sector tenemos una gran responsabilidad al conseguir mejorar el rendimiento de las compañías y a la vez conseguir mejorar la experiencia del cliente.

Las empresas se tienen que dar cuenta que la simple recolección de datos ya no es suficiente, tienen que conseguir aportar un valor a las personas con esa información. Cualquiera que trabaja con datos sabe la dificultad diaria que entrañan muchos de los procesos hasta conseguir una aproximación a un resultado. Parece sencillo pero los procesos para una correcta recolección, tratamiento y explotación del dato son mucho más complejos de lo que parecen.

Pongamos un ejemplo. Una empresa vende aparatos electrónicos en su tienda online. Cada día un número elevado de personas se suscriben a su newsletter porque aparecen ofertas interesantes. Ahí está recogiendo información de los usuarios, en principio solicita un correo electrónico. Ese dato se transmite y se almacena en una base de datos. Un especialista trata esa información para que los datos se transformen en información para el negocio. Eliminar correos duplicados, verificar correos u otros detalles son sólo pinceladas del ingente trabajo que requiere el tratamiento de datos en un proyecto real. Éste es un caso sencillo, imaginad millones de datos, con simbología distinta, desorganizados y desestructurados. Un caos en el mundo organizado.

Con los datos se crean herramientas tecnológicas que ayudan a dinamizar y mejorar nuestras vidas. Si pensamos en música, [Spotify](https://www.northeastern.edu/graduate/blog/spotify-big-data/)
utiliza la explotación de los datos para ofrecerte música que puede gustarte o te avisa de la que escuchan tus amigos. Si pensamos en tecnología, Google, Apple o Microsoft están creando cada día más negocio en base a la información. Si pensamos en los contenidos que consumimos, las plataformas digitales optimizan todos sus procesos en base a datos. Puede sonar pretencioso pero los datos están en muchas de las actividades sociales. En nuestra mano está entenderlos y darles un buen uso.

Los datos están cambiando tu vida. **Es el momento de aprovecharlos**.

[Gonzalo Gete](https://www.linkedin.com/in/gonzalogeteanalistadigital?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BK99f21cySLy19Dv6oFh8CA%3D%3D) junio 2020
