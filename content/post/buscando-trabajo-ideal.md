---
title: "Buscando el trabajo ideal"
date: 2020-07-02T00:21:13+02:00
description: "Historia de los avatares profesionales que surgen en una carrera"
tags: [
    "personas",
    "digital",
    "empresas"
]
type: "post"
image: trabajo_ideal.jpg
---

He sido becario, becario de tarde, en prácticas, contrato en prácticas, productor gráfico, fotógrafo de bodas, creativo digital, consultor, responsable de marketing, digital marketing manager, freelance y analista digital. En algunos sitios incluso he realizado el mismo trabajo.

He sido despedido, me han subido el sueldo, se ha terminado el contrato. Me he ido al primer día: aquello del periodismo económico no era para mí. Lo he dejado por un sitio mejor, ser freelance dura muy poco, mañana no es necesario que vengas. Me he reinventado siete veces y las que quedan por venir.<!--more-->

He estado en empresas muy distintas: medios de comunicación, consultoras, un estudio en [Villacañas,](https://es.wikipedia.org/wiki/Villaca%C3%B1as) start ups, equipos de fútbol, agencias de publicidad etc etc. Las empresas de recursos humanos, recruitment, talent acquisition o simplemente “los que contratan a la peña” deberían prestarme un poco de atención. Tengo un expediente completo.

Utilizando un símil futbolístico, las empresas quieren que te pongas su camiseta y que corras hasta el minuto 90. El esfuerzo no se negocia. Los empleados quieren ser valorados y tener horarios justos que ayuden a conciliar. Unos dan su tiempo y otros pagan por ello.

Conozco pocas empresas en las que todos sus empleados hablen bien de ellas. ¿Tan difícil es conseguir que tus empleados sean tus primeros prescriptores? Hace años no le daba importancia a la empresa que me contrataba, estaba empezando, eran tiempos de crisis económica y lo único que me interesaba era tener un contrato y empezar una carrera profesional.

Recuerdo que una vez tenía un compañero, las oficinas estaban en un piso de Narváez. Nuestra relación se fue enfriando tanto que al final nuestras conversaciones se limitaban al “es para ti”. En ese trabajo aprendí que coger el primer metro de la mañana no tiene nada de romántico y que ser el último en llegar a una empresa puede ser un handicap.

¿Puede haber una sensación más extraña que la de tu primer despido? Piensas, le das vueltas a esa situación. Intentas recomponer como en una película las escenas en las que te has podido equivocar o cuál ha sido el desencadenante. Sientes que eres culpable. Tan culpable como [O.J. Simpson](https://www.youtube.com/watch?v=btf8TBb9DWQ) Es una sensación complicada de afrontar. Siempre recuerdo una frase que me dijeron: “No es culpa tuya. Ese trabajo no te interesaba y te quedan cuarenta años para trabajar”. Así de fácil. Así de sencillo. Ojalá en los momentos de duda llegue siempre alguien y te abra los ojos.

Trabajábamos todos desde casa. Es la empresa más divertida en la que he estado. Nos conectábamos a las reuniones desde cuatro continentes distintos. Las reuniones eran una mezcla de español con palabrotas en inglés como “forwardear”. Todos éramos españoles. Aquí aprendí que trabajar en casa no debería ser trabajar desde que te levantas hasta que te acuestes y que cuando llega un nuevo “head” a la empresa siempre hace cambios. Imperceptibles o no, siempre hace cambios.

En los tiempos de la tecnología no debemos olvidarnos de nuestro componente humano, es nuestra mayor virtud. Las empresas tienen que progresar y los trabajadores debemos ayudarlas. Ahora es un buen momento para replantear las relaciones que estamos construyendo, especialmente en las empresas digitales que es el ámbito que conozco un poco.

A la generación millennial ya no les vale sólo con que los viernes pueda ir de “smart casual” vestido a la oficina. No es suficiente vales de comida a cambio de un contrato de prácticas que incluye el esfuerzo exclusivo de sus mejores años. Ahora los nuevos perfiles demandan el teletrabajo, flexibilidad horaria, comprensión respecto a su trabajo aunque no se entienda y trascender. Pasan un tercio de su tiempo con otra familia. Quieren sentirse al menos igual de bien que cuando llegan con la suya.

Puedo decir que he vivido situaciones parecidas a Mad Men. Una sala, cuatro personas, tres ceniceros. Soltando ideas locas sobre un producto mientras fumábamos (ley antitabaco mediante) sin parar. Puedo decir que nadie tuvo la genial idea de [“It´s Toasted”](https://www.youtube.com/watch?v=E0L8f1IY1Vk) Puedo decir que las empresas modernas, locas y geniales de las series generalmente son otra cosa.

Las empresas las componen muchos elementos, el primordial son las personas. En el futuro cercano como diría mi compañero Juan Capeáns tendremos que dejar paso a los robots y adaptarnos a ellos. Ahora mismo las componen personas.

En los sitios que he estado la gente cuenta sus dudas, algunas veces no hace falta preguntar. Las inquietudes en distintos contextos, empresas y situaciones se repiten:la gente necesita motivaciones y sentirse valorado. Con pequeños gestos creo que se podrían conseguir notables mejoras. Soy creyente de aquellos que piensan que si estás contento en tu puesto de trabajo rindes más.

Aquellos estudiantes a punto de terminar o que se encuentran en su primeras prácticas no deben conformarse. Tienen que saber que no va a ser fácil, que van a tener que luchar, aprender y en muchos momentos abandonar. Habrá gente que te ayude y gente que te tenga miedo porque tienes juventud y ganas.

El camino profesional lo vas creando con tus decisiones. En algunas acertarás y en otras no. Lo que te describe hoy [no será lo mismo en unos años.](https://www.youtube.com/watch?v=DVCfFBlKpN8) Adaptarse a los cambios y arriesgar en algunos momentos puede ser buena opción o no. Dejo que lo averigües por ti mismo.

[**Gonzalo Gete**](https://www.linkedin.com/in/gonzalogeteanalistadigital?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BK99f21cySLy19Dv6oFh8CA%3D%3D) julio 2020
