---
title: "Analista digital: día a día en un ecommerce"
date: 2020-07-23T00:00:13+02:00
description: "Visión personal sobre los acontecimientos que suceden en un día cualquiera en la vida de un analista digital ejerciendo sus funciones dentro de un ecommerce"
tags: [
    "ecommerce",
    "analista digital",
    "google analytics",
    "kpis",
    "tasa de conversión"
]
type: "post"
image: analista_digital_ecommerce.jpg
---

Llega pronto. Todo sucede con mucha rapidez en esas primeras horas de la mañana. Lo primero que mira son las ventas del día anterior. ¿Cómo van respecto al objetivo fijado? ¿Están comparando Like for Like, día contra día? Si las ventas van bien hay una reunión programada con toda seguridad. Si van mal hay dos reuniones programadas.<!--more-->

Revisa sus dashboard personales para ver que todas las webs y sus funcionalidades se están midiendo correctamente. Durante la noche nada se ha caído. Ese momento cuando algo se cae es lo más parecido a un momento de pánico: dudas, frases incómodas, miradas acusadoras y al final, en muchos casos, limpiando caché se resuelve.

Un analista digital tiene que conseguir que con pequeños cambios mejore la rentabilidad de la compañía, que al final es el KPI más importante. Muchas veces se centran en el tráfico y en la tasa de conversión cuando realmente existen otros Kpis que te ayuden más a comprender cómo está funcionando el ecommerce.

Es fundamental conocer muy bien el negocio. Tienes que dominar el peso que tiene la imagen de marca y el peso comercial. Este balance orienta y prioriza muchas de las acciones del analista digital.

Un par de mails por aquí con el proveedor, otro par de mails con gente que quiere presentar herramientas, otro par de mails con el equipo de desarrollo para la última implementación que es muy urgente. Cada día surgen nuevas herramientas que mejoran huecos que ha ido dejando el avance de la tecnología en el sector de la analítica. Un analista tiene que centrarse en las habilidades puesto que las herramientas cambiarán en los próximos años y tendrá que adaptarse. Recomendación para analistas: [Contentsquare.](https://contentsquare.com/es-es/)

Entras en una tienda, ves un producto que te gusta, te lo pruebas y decides que te lo quieres comprar. En el mundo digital este proceso lo podemos trasladar al checkout. Ves un producto en pantalla, miras, o no, las fotos, lo añades al carrito y lo compras. En este sencillo proceso pueden suceder múltiples variaciones, cambios de opinión, dudas con el precio, disconformidad con el método de envío. Un analista debe centrarse cada día al menos un rato en la parte del checkout.

Un analista sueña con KPIS. Los que más atención suelen generar son: tráfico y tasa de conversión. Tráfico: las visitas que acceden a tu web ese día. Tasa de conversión: el ratio de las visitas (una persona puede entrar varias veces) que han entrado en tu web que finalmente realizan una compra. Si de cada cien visitas dos terminan realizando una compra en tu web, la tasa de conversión es del 2%. ¿Son KPIs importantes? Sí. ¿Son los KPIs que miran todos los directores? Sí. ¿Son los KPIs que más importan? No, en mi humilde opinión.

Algunos ecommerce ordenan sus parrillas de producto en base a la inteligencia artificial. Un gran invento. Otros lo ordenan como si de un perchero se tratara. ¿Cuál es la mejor opción de las dos? Depende de muchos factores: imagen de marca, ventas, promociones, producto, stock…En este sector es muy difícil dar recomendaciones generales. Cada ecommerce tiene sus peculiaridades: un tipo de negocio, un perfil de cliente,  un sistema logístico distinto, distintos métodos de pago. Es muy difícil encontrar dos ecommerce que se parezcan demasiado. Casi más importante es saber lo que no tienes que hacer antes que conocer qué puedes hacer.

Hasta ahora parece que el analista digital sólo se dedica a revisar datos. Esto puede convertirse en un error muy habitual. La principal labor de un analista y donde debe aportar más valor a una empresa es en el análisis. Parece una obviedad, sin embargo si las tareas del día día que aportan menos valor colapsan al analista digital su verdadero toque diferencial nunca podrá salir a relucir. El analista digital tiene que detectar errores que puedan suponer mejoras en el rendimiento del ecommerce. Puede realizar análisis de todo lo relacionado con producto, interés de compra, rendimiento de producto, previsiones de compra y así hasta tantas peticiones como reciba.

Baja al mundo de los desarrolladores. Se pone la capa de tecnología e intenta explicar de la mejor forma posible cuál es la necesidad, que alguien del equipo ha trasladado “necesito saber cuantos clics tiene este botón”. Se encarga de preparar un funcional. Una vez implementado hace esa revisión y cuando todo funciona correctamente se sube a producción. Es una parte que a veces no se le da la dimensión que tiene. Sin buenas implementaciones los [análisis estarán siempre cojos.](https://www.davidescolano.es/analitica-digital/tasa-de-rebote-igual-a-cero.html)

El analista es muy importante que hable un idioma universal: el del entendimiento. [Tiene que utilizar la pedagogía.](https://www.youtube.com/watch?time_continue=137&v=WUcbhd1ZUbk&feature=emb_title) Trabaja con personas. Debe saber adaptar el lenguaje dependiendo a quien esté explicando un informe. Si estás hablando con un alto cargo quiere saber los beneficios de la compañía, si hablas con alguien de marketing quiere saber por qué Facebook no está convirtiendo, y si hablas con alguien de desarrollo tienes que hacerle entender que esa implementación es importante para medir el rendimiento de un nueva funcionalidad.

Algunas compañías han dividido su web en cuatro apartados para intentar potenciar con equipos transversales las distintas áreas. El analista tendrá unas funciones dependiendo si trabaja en la parte del checkout, la home, las secciones del cliente o la ficha de producto y parrillas. Otras compañías trabajan con dos equipos: uno se encarga de analizar el ahora, midiendo y ejecutando diariamente, y el otro se encarga de planificar la próxima temporada/año, con una parte más estratégica, recibiendo datos y aprendiendo de errores del pasado. Hay empresas en las que el analista digital trabaja con todos los departamentos “clásicos”. En otras está integrado en el departamento de marketing e incluso en algunas está en el departamento de desarrollo.

El analista tiene que dedicar una parte de su día a pensar, como la mayoría de las personas que trabajan en el sector digital. Para, reflexiona y piensa. Ir por impulsos e intuición apretando a otros compañeros no suele dar buen resultado. El analista observa la situación y luego decide cuál es el mejor camino con las herramientas que posee.

Esto es un poco la teoría de lo que debe ocurrir. En la realidad cada compañía es un mundo, con sus propias contradicciones. Hice lo que pude, es el epitafio de un buen analista digital.
