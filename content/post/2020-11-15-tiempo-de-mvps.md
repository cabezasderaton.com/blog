---
title: Tiempo de MVPs
description: Corren tiempos complejos. Y en el mundo digital, los mejores
  "players" son los que son capaces de reducir riesgos y mejoras las
  expectativas de éxito. Hoy hablamos de MVPs.
date: 2020-11-15T17:02:11.616Z
draft: false
tags:
  - MVP
  - estrategia
  - digital
  - prototipo
image: mvp-cabezas-de-raton-blog-estrategia-digital-prototipos-juan-capeans.png
type: post
---
Para muchos el MVP es el mejor jugador de un partido, Most Valuable Player por sus siglas en inglés. Para los conocedores del mundillo técnico, sin embargo, hace referencia a un  prototipo, MVP = Producto Mínimo Viable por sus siglas en inglés: *Minimum Viable Product*. Hoy vamos a hablar un poco sobre este concepto que encierra un cambio de filosofía para las empresas que tratan de adaptarse al mundo moderno.

Después de muchos años en el ámbito digital, parece que da un poco de pereza tocar este tema porque para empresas tecnológicas es una de las estrategias más seguidas. Pero en el ámbito de empresas tradicionales en las que la digitalización es un reto mayúsculo, no está demás repasar este concepto. Una vez más, es aplicar la lógica a los procesos, pero normalmente las empresas más tradicionalistas no conciben que un proyecto imperfecto puede ser la mejor de las estrategias en el ámbito de Internet. Y que incluso puede llegar a salvarlas.

Los MVPs son un término recurrente en el ámbito digital que hace referencia a la primera versión de tu producto que debes producir, que tiene que ser muy sencilla, con una funcionalidad y un propósito muy claros, para testearlo ante tu audiencia. Como el desarrollo de un prototipo puede ser complejo y siempre tiene unos costes, se recomienda adelgazar al máximo ese prototipo hasta que te quedes con la funcionalidad básica y así puedas medir de verdad la respuesta de tu audiencia ante tu producto. Si tu app o producto digital tiene una buena acogida, entonces debes seguir desarrollándola y añadiendo nuevas funcionalidades. Pero si al público no le gusta, o no sabes llegar a él, entonces debes enfocar tus esfuerzos a otros ámbitos lejos del desarrollo. Y, en último caso, ya sabéis lo que decía Napoleón: “una retirada a tiempo es una victoria”. No tiene ningún sentido pasarte 3 años desarrollando una mega-app, cuando podías haber hecho un prototipo en 3 meses que realmente te indicase si a la gente le gusta o no esa aplicación.

![en que consiste un Producto Minimo Viable](/images/que-es-un-minimum-viable-product-producto-minimo-viable-cabezas-de-raton-blog-estrategia-digital.jpg "Que es un MVP")

Todo esto deriva de la metodología [Lean Startup](https://robertotouza.com/lean-startup/the-lean-startup-el-libro-para-aprender-a-crear-startups/), que viene a ser una metodología ágil de trabajo aplicable a mucho ámbitos profesionales, pero que hoy en día es el Santo Grial en el desarrollo tecnológico. Podéis leer mucho más sobre Lean Startup en la [Wikipedia](https://es.wikipedia.org/wiki/Lean_startup) y en multitud de libros que hablan sobre esta teoría empresarial y de desarrollo tecnológico.

Una vez que ya sabemos de qué va esto del MVP, hay que ponerse manos a la obra. Pero, ¿por dónde empezamos? ¿qué forma tiene un MVP? ¿qué hay que enseñar a nuestros usuarios? Pues para todo hay teorías y ejemplos. Lo que empezó siendo un simple prototipo, al cabo de un tiempo los americanos lo dieron en llamar una “vertical slice” haciendo referencia a un corte transversal de una tarta.

![ capas de un producto minimo viable o  MVP - Cabezas de Raton](/images/capas-de-un-mvp-prototipo-cabezas-de-raton-blog.png "capas de un producto minimo viable o  MVP")

Pero así que se empezó a generalizar esta fórmula de trabajo, alguien muy listo se dio cuenta de que un corte transversal no te da en absoluto toda la información necesaria. Es más, en cuanto los usuarios de apps empiezan a tener una visión más crítica del mercado, no todo se puede ceñir a la parte técnica. El diseño y la usabilidad ganan más peso, así que ese alguien crea el siguiente cuadro, dando importancia a otros aspectos:

![Condiciones de un MVP o Producto Minimo Viable](/images/que-es-un-mvp-cabezas-de-raton-blog-estrategia-digital-juan-capeans.png "Que es un MVP")

En la imagen de arriba, se ve como un prototipo no debe ser la base del producto, si no que tiene que tener un poco de todo: diseño, usabilidad, fiabilidad y funcionalidad. Porque los usuarios ya tienen mucho criterio y no se admiten productos (por muy prototipos que sean) que no les entren por los ojos o que funcionen de una manera muy tosca. Como todo, el concepto de MVP ha tenido que ir evolucionando, porque la industria ya no es la misma desde el lanzamiento del primer iPhone allá por aquel lejano junio del 2007. Al principio valía casi todo, pero así que el mercado gana en madurez, los usuarios se vuelven mucho más estrictos y exigen un nivel de calidad mucho más alto. Y esto, aunque suponga un trabajo añadido, es muy bueno para todo el ecosistema. Porque como usuarios tendremos mejores servicios y como “productores” tendremos a una audiencia mucho más fiel a nuestro producto, porque se adapta a los gustos de la audiencia. 

Y para rizar el rizo, a finales del 2014, un tipo más listo todavía dice que el vertical slice ya no tiene sentido, que está obsoleto. Que viendo un corte de una tarta no te imaginas bien cómo era esa tarta. Así que hace un pequeño cambio, con mucha maestría y dice que a partir de ahora nada de porciones de tarta, a partir de ahora lo que hay que hacer son cupcakes… y lo da en llamar Minimum Loveable Product: MLP, es decir, el Producto Mínimo que te Enamorará. Quitando la parte cursi del tema, es un cambio que tiene sentido. Cuando te decides a sacar un prototipo al mercado, tiene mucho sentido extrapolar el concepto “cupcake” y que al usuario le entre por la vista, que vea una miniatura de lo que puede llegar a ser, pero con todos los ingredientes, incluido el diseño.

![cupcake vs tarta: optimizacion de MVPs](/images/mvp-vs-mlp-como-mejorar-tus-prototipos-de-producto-digital-cabezas-de-raton-blog-estrategia-digital.png "cupcake vs porcion de tarta")

Y es que sólo viendo la porción, no te acabas de imaginar bien cómo será el producto final. Con el cupcake, parece que todo tiene más sentido. Una pena que a mí personalmente los cupcakes no me gusten nada. Excesivamente dulces…

Si os interesa este tema, os dejo un artículo super-interesante en el que os podréis enterar de [cómo preparar bien vuestro MLP](http://bit.ly/1r5LZ6f)

Por último solo queda hacer una pequeña reflexión de la importancia de esta filosofía en tiempos actuales en los que la incertidumbre es la norma. Ya hablamos de eso en nuestro primer artículo [Vivir en Beta](https://cabezasderaton.com/post/vivir-en-beta/) pero la crisis sanitaria y económica derivada del Covid-19, hace que las estrategias tradicionales ya no sirvan. Necesitamos adaptarnos en semanas y dar respuesta a necesidades que el mes anterior no teníamos. Es imposible hacerlo con las metodologías clásicas y planificaciones a 5 años. ¿Alguien es capaz de predecir las tiendas que se abrirán o cerrarán el próximo año? Imaginaos intentar seguir hojas de ruta a varios años vista. A día de hoy es impensable. Y las metodologías ágiles dan respuesta a cómo afrontar estos tiempos de incertidumbre en el ámbito empresarial. Eso sí, sólo valen si de verdad se asumen los compromisos que conllevan y si toda la empresa, desde el director general hasta la última pieza del engranaje, están dispuestos a asumirlos. Si no, mejor seguir con el ordeno y mando, que nos ahorramos todos mucho trabajo :)

[](https://www.linkedin.com/in/juan-capeans/)

[Juan Capeáns](https://www.linkedin.com/in/juan-capeans/),\
noviembre 2020