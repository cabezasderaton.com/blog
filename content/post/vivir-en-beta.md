---
draft: false
author:
  - Juan Capeáns
title: Vivir en Beta
description: El mundo digital post Covid tendrá que adaptarse a una realidad de
  productos imperfectos
date: 2020-05-28T00:21:12+02:00
tags:
  - estrategia digital
  - Covid19
  - beta
  - estrategia
  - digital
image: incertidumbre.jpg
type: post
---

Mucho se ha escrito estas semanas sobre la vida post-Covid19. Yo voy a hacer una reflexión global que gira sobre un principio básico: **adaptación al medio**. La única predicción en la que creo firmemente es que a partir de ahora debemos asumir como natural algo a lo que el ser humano suele tener rechazo: la incertidumbre.
<!--more-->

Esta reflexión surge desde alguien que vive muy metido en el ecosistema digital, pero con gran querencia hacia el negocio “offline”. Está claro que esta crisis no va a afectar del mismo modo a un programador web que a una dependienta de una tienda tradicional, pero sí que el tablero de juego habrá cambiado para todos. En este ejemplo, el programador juega en casa y le toca al negocio tradicional tratar de adaptarse al nuevo escenario.

¿Por qué hablo de Vivir en Beta? Porque nos va a tocar movernos en la incertidumbre, estaremos en una fase de *testing* continua. Pasamos de un escenario medianamente controlado a uno en el que la inseguridad es la única certeza. Ya no tienes tan claro cómo se va a comportar el cliente después del verano, qué esperar de las próximas navidades y no puedes hacer comparativas con el mismo día del año anterior. No hay [“like for like”](https://lexetsocietas.com/2019/06/01/que-se-entiende-por-like-for-like-lfl/) en las empresas para comparar las ventas de ayer con el mismo día del año anterior. Más concretamente, no tiene validez, puesto que en mayo del año pasado no estábamos bajo una pandemia global ni llevábamos dos meses confinados en casa. Pero está claro que vamos a seguir necesitando indicadores de negocio. Habrá que hilar mucho más fino en las cuentas, olvidarse de objetivos puestos con anterioridad y redefinir esos objetivos y KPIs.

Es posible que tu empresa sólo quiera superar esta crisis sin sufrir grandes traumas. O a lo mejor aprovecha para transformarse, o espera a que escampe siendo muy precavido en los gastos, o diversifica... Hay muchos escenarios abiertos, y todos ellos pueden ser válidos. Ya sabemos que no hay recetas mágicas aplicables a todas las empresas, así que toca un momento de introspección. Después de 60 días de encierro forzoso en casa, las cabezas pensantes de cada organización ya han debido hacer los deberes y buscado soluciones que se adapten al momento y la idiosincrasia de su organización. Ahora toca poner en práctica esas reflexiones, que es el momento en el que realmente llegan los problemas. El papel o el Excel, lo aguantan todo... Esta frase anónima lo resume de manera maravillosa:

> “In theory there is no difference between theory and practice. But, in practice, there is.”

Que podríamos traducir como:
> “En teoría, no hay diferencia entre teoría y puesta en práctica. Pero en la práctica, sí hay diferencia”. Y mucha, añadiría yo.

Si las organizaciones apuestan por la digitalización estarán entrando a un universo que ya no es ajeno a nadie pero que se ha acelerado enormemente durante el confinamiento. ¡Menos mal que teníamos todo tipo de servicios de ocio online estas últimas semanas!
Lo malo es que la mayoría de organizaciones entra a la digitalización como iban los buscadores de oro en busca de El Dorado: sólo se ve la parte positiva. Pero una buena estrategia digital dista mucho de meter dinero a contratar servicios, plataformas y un par de frikis que se encarguen de todo. Conlleva un cambio de chip. Y ahí es donde cobra importancia la adaptación al medio. Hay que aprender a convivir en un ecosistema imperfecto en el que la prueba y error se vuelve tu día a día. Donde los productos se ponen en producción a sabiendas de que distan mucho de ser perfectos. Donde no conoces la reacción del usuario/cliente. Donde debes ser ágil para analizar la respuesta del público y seguir evolucionando los productos y servicios que más gustan. Pero sobre todo abandonar ideas que no tienen aceptación. No emperrarse. No creerte que conoces a los cientos/miles/millones de usuarios que van a entrar por tu escaparate digital, porque realmente no tienes ni idea de cómo se van a comportar.

Y la adaptación a ese mundo de incertidumbre es complicado. Cómo le dices a alguien que lleva un negocio del mismo modo que lo hacían sus padres y abuelos, que ahora en el mundillo online vale más la prueba-error que la forma tradicional de ofrecer el producto. En un experimento de laboratorio con ratas, J Weiss descubrió que la incertidumbre era un posible motor del estrés, y que éste puede tener consecuencias devastadoras. En este artículo de José Luis González de Rivera tenéis información muy interesante sobre el tema [en este enlace](http://www.psicoter.es/pdf/89_C018_01.pdf)  (el experimento con ratones en la página 7).

Hay un ejemplo muy gráfico en el mundo de los videojuegos que ilustra la vida en Beta: durante el siglo XX un juego tardaba mucho tiempo en ponerse a la venta desde el momento en el que se acababa de programar. Llegaba un período de tiempo de testear el juego, distribuyéndolo físicamente a unos pocos usuarios para recoger feedback y corregir los fallos. Podían tardar meses o incluso años hasta que el juego salía a la venta. Sólo había que vigilar que no se piratease en exceso, aunque no estaba ni mucho menos tan extendido el pirateo (aunque por suerte cada vez se piratea menos, al menos en España).
Desde aquel mundo calmado, que bebía de las normas de la vieja escuela y que tan buenos resultados arrojó, ahora pasamos a un mundo más “alocado” en el que la entrega temprana de valor al usuario es la máxima a pesar de tener que sacrificar aspectos que hasta hace unos años eran vitales. Podéis echar un vistazo a los [Principios del Manifiesto Ágil](https://agilemanifesto.org/iso/es/principles.html) para ver qué normas rigen ahora la industria del software. Una locura para los viejos estándares de calidad.

En el desarrollo de un videojuego, hoy se sacan versiones beta a sabiendas de que pueden dar fallos, algunos graves, con tal de ofrecerle al usuario el producto de manera rápida. Se busca aportar valor y hacer partícipe al usuario en el propio desarrollo a través de la comunidad online creada alrededor del videojuego. De este modo, reportará problemas (bugs) que los desarrolladores irán corrigiendo y aportará ideas que puedan cambiar incluso una parte del argumento. Ha pasado de ser un producto cerrado en el que la máxima era que el juego no tuviese fallos, a un producto en el que los pequeños fallos no son tan malos siempre que aportes valor, rapidez y autenticidad a tu comunidad, puesto que tus usuarios (cada vez más en el papel de fans que en el de meros clientes) sentirán el producto como algo propio, que está vivo. Eso lleva a nuevas formas de monetización: se venden actualizaciones, nuevos mapas/fases e incluso merchandising y experiencias alrededor de un juego que pasa a ser mucho más que un simple “mata-marcianos” o como decía el gran Forges: [“matamuch”](https://vandal.elespanol.com/noticia/1350655361/el-dibujante-forges-ridiculiza-la-idea-del-videojuego-como-producto-cultural/).

El cambio de chip en la industria del videojuego ha sido espectacular. Y al que vea con muy buenos ojos el poder monetizar mejor al usuario a lo largo del tiempo en lugar del “one-shot” antiguo en el que el usuario se compraba el juego y ahí se acababa la relación con el desarrollador, tiene que asumir también el nuevo paradigma de incertidumbre. No es fácil, y a lo mejor tampoco vale para todas las empresas. Pero el público cada vez demanda organizaciones más ágiles. Vivimos en el mundo de la inmediatez y para ello hay que aprender a priorizar. Y ahí es dónde sale a relucir el talento. Pero eso ya lo dejamos para un nuevo artículo.

Los [resultados en la industria de los videojuegos](http://www.magazinedigital.com/historias/reportajes/videojuegos-una-industria-135000-millones) demuestran que ha funcionado. Han superado a la industria del cine en facturación y la gamificación lo está inundando todo. Pero no tiene por qué ser una respuesta válida para todas las organizaciones. Y no todo el mundo vale para ello, ni mucho menos.

Curiosamente en el ámbito del desarrollo tecnológico se lleva muy mal la incertidumbre en el día a día. Un desarrollador (programador) quiere una hoja de ruta lo más precisa posible. Una especificación clara de lo que tiene que hacer, sin cabos sueltos ni interpretaciones libres. Instrucciones claras, concisas y precisas. En definitiva, no quiere ninguna incertidumbre para moverse en un terreno incierto. Curioso, ¿verdad? **¡Bienvenidos a la Vida en Beta!**




[Juan Capeáns](https://www.linkedin.com/public-profile/settings?trk=d_flagship3_profile_self_view_public_profile&lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_self_edit_contact_info%3BAaqwLIv6RzG41A%2BjKc%2Bubw%3D%3D)
mayo 2020
