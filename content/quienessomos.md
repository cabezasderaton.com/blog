---
date: 2019-05-28
type: section
title: "Así nacimos"
---

Esto empieza como cualquier proyecto entre dos amigos. Una broma,  dos horas de conversación y finalmente un blog. Una empresa juntó nuestras carreras, ahora este blog pretende acercaros aquellas ideas, visiones o conocimientos que podamos tener de nuestra experiencia en distintas empresas y proyectos en el mundo digital. Queremos ser didácticos, amenos y en algunos momentos divertidos. Esperamos que os sintáis parte y os animéis a participar y darnos vuestra visión de los temas que planteamos.


Queremos entretener y, si en algún caso podemos, dar una visión diferente o compartir enseñanzas. No nos van las fórmulas mágicas ni creemos saberlo todo, ni mucho menos. Somos Juan Capeáns y Gonzalo Gete:


[Juan Capeáns](https://www.linkedin.com/in/juan-cape%C3%A1ns-84251310/) tras varias experiencias de emprendimiento y varios ecommerce a sus espaldas, termina trabajando en Barcelona en una empresa referente en el desarrollo de apps. De ahí salta a la plataforma líder en el ámbito del podcast en español, ¿habrá llegado el momento de lanzar un podcast tras tanto tiempo dándole vueltas? Veremos... Tras bregarse duro en el ámbito startup le llega la oportunidad de volver a su tierra, Galicia, para sentar las bases para el crecimiento de las aplicaciones móviles en el ámbito de la moda.. Uniendo su pasión por el mundo online con su vocación por el mundo offline (es hijo, nieto y bisnieto de tenderos), acaba dando el salto a la omnicanalidad, trabajo que desempeña en estos momentos.



[Gonzalo Gete](https://www.linkedin.com/in/gonzalogeteanalistadigital?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base_contact_details%3BK99f21cySLy19Dv6oFh8CA%3D%3D) ha ido saltando de consultoras a agencias de publicidad o startups buscando siempre mejorar su conocimiento digital. En Madrid, su último trabajo en un buscador de espacios para eventos le abre la posibilidad de conocer Galicia gracias a la oferta de trabajo en una empresa textil. Desde abajo intenta implantar una cultura de toma de decisiones basadas en datos en un mundo de imagen. Un largo camino que continúa en el presente.

Pero no queremos que este proyecto sea personalista, así que invitamos a todo aquel que tenga algo que contar a que nos comente su idea y, si está dotado con un don para la escritura, le invitamos a participar en este pequeño rincón de pensar. Este proyecto nace en pleno confinamiento por el Covid-19, así que veremos a dónde nos lleva la marea.


Como decía Cyrano de Bergerac:
> “quizá no volar muy alto, pero libre”.

¡Bienvenidas sean todas las cabezas de ratón que habitan el mundo!
